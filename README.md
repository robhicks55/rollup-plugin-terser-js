rollup-plugin-terser-js
=======================

This is a thin cjs wrapper around [terser's](https://github.com/fabiosantoscode/terser) [API](https://github.com/fabiosantoscode/terser#api-reference).

Rollup accepts an array of plugins:
```Javascript
const rollup = require('rollup');
const terser = require('rollup-plugin-terser-js');

rollup({
  entry: 'main.js',
  plugins: [ terser([{terser api options}]) ]
}).then(...);
```
