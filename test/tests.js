const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;
const assert = chai.assert;
const root = process.cwd();
const { resolve } = require('path');

const rollup = require('rollup');
const terser = require('../index.js');
const fs = require('fs-extra');

let inputOptions = {plugins: [terser()]};
let outputOptions = {format: 'es'};

describe('rollup-plugin-terser-js', () => {
	it(`should minify`, async () => {
		inputOptions.input = resolve(root, 'test', 'mocks', 'Foo.js');
		const bundle = await rollup.rollup(inputOptions);
		const { code } = await bundle.generate(outputOptions);
		expect(code).to.be.a("string");
		expect(code).to.not.include('\t');
	});
});
