/**
 * Determines if an object is a function
 * @param  {object}  obj Object to test
 * @return {Boolean}     True if the object is a function. Otherwise false
 */
export function isFunction(obj) {
  return typeof obj === 'function';
}
