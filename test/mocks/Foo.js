import { isJson } from './isJson.mjs';
import { isElement } from './isElement.mjs';

export class Foo {
  constructor(str) {
    this.isJson = isJson(str);
    this.element = isElement({});
    this.template = `
    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css" />
      <link rel="stylesheet" href="/bulma/css/bulma.css" />
      <link rel="stylesheet" href="css/master.css" />
      <style>${css}</style>
      <div class="modal is-active">
        <div class="modal-background"></div>
        <div class="modal-card">
          <header class="modal-card-head">
            <p class="modal-card-title">
              <slot name="heading"></slot>
            </p>
          </header>
          <section class="modal-card-body">
            <slot name="body">
          </section>
          <footer class="modal-card-foot">
            <slot name="footer"></slot>
          </footer>
        </div>
      </div>
      `;
  }
}

export { isFunction } from './isFunction.mjs';
export { isObject } from './isObject.mjs';
export { isSpecial } from './isSpecial.mjs';
