/**
 * Class to build strings, with fluent interface
 * @type {String}
 */
export class StringBuilder {
  constructor(string = '') {
    this.string = String(string);
  }

  /**
   * Returns the string that has been built
   * @param  {Boolean} [camelCaseIt=false] Whether to camelCase the string
   * @return {String}                      The string that has been built.
   */
  toString(camelCaseIt = false) {
    return camelCaseIt ? this.string.replace(/(\-\w)/g, m => m[1].toUpperCase()) : this.string;
  }

  /**
   * Append a string to the string
   * @param  {String} val the string to append
   * @return {Instance} the instance for chaining
   */
  append(val) {
    this.string += val;
    return this;
  }

  isEmpty() {
    return this.string === '';
  }

  /**
   * Insert a string a particular position
   * @param  {Integer} pos Position at which to insert string
   * @param  {String} val String to insert
   * @return {Instance} the instance for chaining
   */
  insert(pos, val) {
    let length = this.string.length;
    let left = this.string.slice(0, pos);
    let right = this.string.slice(pos);
    this.string = left + val + right;
    return this;
  }
}
