const minify = require('terser').minify;

module.exports = function terser(options = {}, minifier = minify) {
	return {
		name: 'terser',

		transformChunk(code) {
			const result = minifier(code);
			return result;
		}
	};
}
